<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductosController extends Controller
{
    public function index (){
        return view('productos.index');
    }
    
    public function create (){
        return 'Formulario para crear lista con controlado';
    
    }

    public function store(){
    }

    public function show($productos){
        return view('productos.show');
    }

    public function edit($productos){
        return "Mostrando el formulario para editar Productos con el ID {$productos}";
    }

    public function update($productos){
     
    }

    public function delete($productos){
       
    }
}
