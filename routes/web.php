<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','MainController@index')-> name('main');

//mostrar lista de productos
Route::get('productos', 'ProductosController@index')-> name('productos.index');

//Crear lista de productos
Route::get('productos/create', 'ProductosController@create')-> name('productos.create');

//Crear lista de productos metodo post
Route::post('productos', 'ProductosController@store')-> name('productos.store');

//introducir parametros en las rutas
Route::get('productos/{productos}','ProductosController@show')-> name('productos.show');

//Editar lista de productos
Route::get('productos/{productos}/edit','ProductosController@edit')-> name('productos.edit');

//Actualizar productos
Route::match (['put', 'patch'], 'productos/{productos}','ProductosController@update')-> name('productos.update');

//Eliminar productos
Route::delete('productos/{productos}','ProductosController@destroy')-> name('productos.destroy');